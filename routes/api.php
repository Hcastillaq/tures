<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(['prefix'=>'administrador'], function(){
	Route::post('todos', 'AdministradorController@index');
	Route::post('crear', 'AdministradorController@store');
	Route::post('ver/{id}', 'AdministradorController@show');
	Route::post('editar/{id}', 'AdministradorController@update');
	Route::post('eliminar/{id}', 'AdministradorController@destroy');
});

Route::group(['prefix'=>'galeria'], function(){
	Route::post('todos', 'GaleriaController@index');
	Route::post('crear', 'GaleriaController@store');
	Route::post('ver/{id}', 'GaleriaController@show');
	Route::post('editar/{id}', 'GaleriaController@update');
	Route::post('eliminar/{id}', 'GaleriaController@destroy');
});

Route::group(['prefix'=>'hitos'], function(){
	Route::post('todos', 'HitoController@index');
	Route::post('crear', 'HitoController@store');
	Route::post('ver/{id}', 'HitoController@show');
	Route::post('editar/{id}', 'HitoController@update');
	Route::post('eliminar/{id}', 'HitoController@destroy');
});

Route::group(['prefix'=>'imagenes'], function(){
	Route::post('todos', 'ImagenController@index');
	Route::post('crear', 'ImagenController@store');
	Route::post('ver/{id}', 'ImagenController@show');
	Route::post('editar/{id}', 'ImagenController@update');
	Route::post('eliminar/{id}', 'ImagenController@destroy');
});

Route::group(['prefix'=>'mensajes'], function(){
	Route::post('todos', 'MensajeController@index');
	Route::post('crear', 'MensajeController@store');
	Route::post('ver/{id}', 'MensajeController@show');
	Route::post('editar/{id}', 'MensajeController@update');
	Route::post('eliminar/{id}', 'MensajeController@destroy');
});

Route::group(['prefix'=>'sitio'], function(){
	Route::post('crear', 'SitioController@store');
	Route::post('ver', 'SitioController@show');
	Route::post('editar', 'SitioController@update');
	Route::post('eliminar/{id}', 'SitioController@destroy');
});

Route::group(['prefix'=>'tures'], function(){
	Route::post('todos', 'TourController@index');
	Route::post('crear', 'TourController@store');
	Route::post('ver/{id}', 'TourController@show');
	Route::post('editar/{id}', 'TourController@update');
	Route::post('eliminar/{id}', 'TourController@destroy');
});

Route::group(['prefix'=>'infoContacto'], function(){
	Route::post('info', 'InfoContactoController@index');
	Route::post('crear', 'InfoContactoController@store');
	Route::post('ver/{id}', 'InfoContactoController@show');
	Route::post('editar/{id}', 'InfoContactoController@update');
	Route::post('eliminar/{id}', 'InfoContactoController@destroy');
});
