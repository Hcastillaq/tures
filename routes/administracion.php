<?php

    use App\Http\Middleware\AuthAdmin  as AuthAdmin;
    use Illuminate\Support\Facades\Auth;
    
    Route::group(['prefix'=>'administracion'], function(){

        Route::middleware([AuthAdmin::class])->group(function(){
            Route::get('/', function(){
                return redirect('/administracion/tures');
            });
    
            Route::get('/tures', function(){
                return view('administracion.paginas.inicio');
            });
    
            Route::get('/informacion', function(){
                return view('administracion.paginas.informacion');
            });
    
            Route::get('/mensajes', function(){
                return view('administracion.paginas.mensajes');
            });
    
            Route::get('/administradores', function(){
                if(Auth::user()->range != 0){
                    return redirect('/administracion');
                }
                return view('administracion.paginas.administradores');
            });
    
            Route::get('/galeria', function(){
                return view('administracion.paginas.galeria');
            });
        });

        Route::get('/entrar', function(){

            if(Auth::check()){
                return redirect('/administracion');
            }
            return view('administracion.paginas.auth.login');
        });

        Route::post('/entrar', 'AuthController@login');
        Route::get('/salir', 'AuthController@logout');
        
    });
