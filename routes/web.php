<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\InfoContactoController as info;
use App\Http\Controllers\SitioController as site;
use App\Http\Controllers\GaleriaController as Galeria;
use App\Http\Controllers\TourController as Tour;
use Illuminate\Http\Request;

include 'administracion.php';

Route::get('/', function () {
    $info = new info();
    $site = new site();
    $galeria = new Galeria();

    return view('inicio.paginas.inicio', 
        [
            'info' => $info->index(), 
            'sitio' => $site->show(),
            'galeria' => $galeria->index()
        ]);
});

Route::get('/tours', function (Request $request) {

    $origen = $request->input('origen');
    $destino = $request->input('destino');
    $tours = [];

    if(is_null($origen) || empty($origen) && is_null($destino) || empty($destino)) {
        $tours = App\Tour::with(['imagenes'=>function($q){
            $q->orderBy('id', 'DESC');
        }])->get();
    }

    if(!is_null($origen) && !empty($origen)){
        $tours = App\Tour::where('origen', 'LIKE', "%$origen%");
        
        if(!is_null($destino) && !empty($destino)){
            $tours->where('destino', 'LIKE', "%$destino%");
        }

        $tours = $tours->get();
    }else{
        $tours = App\Tour::where('destino', 'LIKE', "%$destino%")->get();
    }

    

    return view('tures.paginas.inicio', [
            'tours' => $tours,
            'origen' => $origen,
            'destino' => $destino
        ]);
});

Route::get('/tours/{id}', function ($id) {
    
    $tours = App\Tour::with(['imagenes'=>function($q){
        $q->orderBy('id', 'DESC');
    }])->find($id);
   
    return view('tures.paginas.vtures', ['tour' => $tours]);
});

