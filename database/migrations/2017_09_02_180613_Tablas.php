<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tablas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 50);
            $table->string('precio', 20);
            $table->string('origen', 100);
            $table->string('destino', 100);
            $table->string('mapa', 500);
            $table->timestamps();
        });

        Schema::create('hitos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 255);
            $table->string('descripcion', 255);
            $table->integer('tour_id')->unsigned();
            $table->foreign('tour_id')->references('id')->on('tours')
                ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('imagenes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 255);
            $table->integer('tour_id')->unsigned();
            $table->foreign('tour_id')->references('id')->on('tours')
                ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('sitio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nosotros', 500);
            $table->string('imgLogo', 100);
            $table->timestamps();
        });

        Schema::create('administradores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 150);
            $table->string('usuario', 150)->unique();
            $table->string('password', 150);
            $table->integer('range')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('galeria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('urlImagen', 100);
            $table->integer('sitio_id')->unsigned()->nullable();
            $table->foreign('sitio_id')->references('id')
                ->on('sitio')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('infoContacto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('telefono', 60);
            $table->string('correo', 150);
            $table->text('direccion');
            $table->integer('sitio_id')->unsigned()->nulleable();
            $table->foreign('sitio_id')->references('id')
                ->on('sitio')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('mensajes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 45);
            $table->string('correo', 150);
            $table->string('mensaje', 450);
            $table->integer('sitio_id')->unsigned()->nullable();
            $table->foreign('sitio_id')->references('id')
                ->on('sitio')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tablas');
    }
}
