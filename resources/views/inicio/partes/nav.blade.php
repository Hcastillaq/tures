<nav-component url="/" :clase="color" logo="{{asset('imagenes/sitio')}}/{{$sitio->imgLogo}}">
    <div slot="uno">
        <ul id="nav-mobile" class="right hide-on-small-only">
            <li>
                <a href="#home">
                    <span class="link" active='#home'>Inicio</span>
                </a>
            </li>
            <li>
                <a href="/tours">
                    <span class="link">Tours</span>
                </a>
            </li>
            <li>
                <a href="/#nosotros">
                    <span class="link" active='#nosotros'>Nosotros</span>
                </a>
            </li>
            <li>
                <a href="#galeria">
                    <span class="link" active='#galeria'>Galeria</span>
                </a>
            </li>
            <li>
                <a href="/#contacto">
                    <span class="link" active='#contacto'>Contacto</span>
                </a>
            </li>
        </ul>
    </div>

    <div slot="dos">
        <ul id="nav-mobile" class="navMobile">
            <li>
                <a href="#home">
                    <span class="link" active='#home'>Inicio</span>
                </a>
            </li>
            <li>
                <a href="/tours">
                    <span class="link">Tours</span>
                </a>
            </li>
            <li>
                <a href="/#nosotros">
                    <span class="link" active='#nosotros'>Nosotros</span>
                </a>
            </li>
            <li>
                <a href="#galeria">
                    <span class="link" active='#galeria'>Galeria</span>
                </a>
            </li>
            <li>
                <a href="/#contacto">
                    <span class="link" active='#contacto'>Contacto</span>
                </a>
            </li>
        </ul>
    </div>
</nav-component>