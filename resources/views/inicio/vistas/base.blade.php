<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('tituyietuldlo')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @include('inicio.partes.links')
</head>
<body>
    <div id="loadin_page">
        <img src="{{asset('imagenes/sitio')}}/loader.gif">
    </div>
    <div id="app" class="home">
            @include('inicio.partes.nav')
            <div class="app" id="home">
                <div class="container">
                    <div class="index-banner">
                        <h1>Over Alestur</h1>
                        <a href="/tours" class="btn1 right">
                        <span class="left title">Tours</span>
                        <span class="right icon fa fa-ship"><span class="arrow-right"></span></span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="container-fluid info-nosotros" id="nosotros">
                <div class="row">
                    <div class="col xl6 l6 m6 s12 info-card">
                        <h1>NOSOTROS</h1>
                        <div class="icons">
                            <div class="ico"><a href="#"><i class="fa fa-facebook-square"></i></a></div>
                            <div class="ico"><a href="#"><i class="fa fa-twitter"></i></a></div>
                            <div class="ico"><a href="#"><i class="fa fa-youtube-play"></i></a></div>
                        </div>
                    </div>
                    <div class="col xl6 l6 m6 s12 info-card1">
                        @if(!empty($sitio->nosotros))
                        <span>{{$sitio->nosotros}}</span>
                        @endif
                    </div>
                </div>
            </div>
            
            <div class="color">
            <div class="container-fluid" id="galeria">

                <div class="row section-tours">
                    <carrusel-component :galeria="{{$galeria}}" 
                        :tiempo="3000" :prefix="'galeria/'">
                    </carrusel-component>

                    <lightbox-component :bus="busData">
                    </lightbox-component>
                </div>

                <div class="row section-info" id="contacto">
                    <div class="col m4 s12 section-contac">
                        <h3>Horarios</h3>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam animi eum tenetur.</span>
                        <div>
                            <h3>Cont&aacute;ctenos</h3>
                            @if(!empty($info))
                                <h5><i class="fa fa-phone"></i><a href="tel:{{$info->telefono}}">&nbsp;{{$info->telefono}}</a></h5>
                                <h5><i class="fa fa-envelope"></i><a href="mailto:{{$info->correo}}">&nbsp;{{$info->correo}}</a></h5>
                            @endif
                        </div>
                        <div class="icons">
                            <div class="ico"><a href="#"><i class="fa fa-facebook-square"></i></a></div>
                            <div class="ico"><a href="#"><i class="fa fa-twitter"></i></a></div>
                            <div class="ico"><a href="#"><i class="fa fa-youtube-play"></i></a></div>
                        </div>
                    </div>

                    <div class="col m4 s12 section-mensaje">
                        
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="col s12">
                                        <h3>Escr&iacute;benos</h3>
                                    </div>
                                </div>
                                
                                <contacto-component>
                                </contacto-component>
                            </div>
                            
                        </div>
                        

                    </div>

                    <div class="col m4 s12 section-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15652.897010839391!2d-74.2120741!3d11.2449071!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf1efa0513ab01925!2sAlestur+Ltda!5e0!3m2!1ses-419!2sco!4v1502562880904" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <footer-component></footer-component>
        </div>
    </div>
</body>
</html>

@include('inicio.partes.scripts')
