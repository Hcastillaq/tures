@extends('administracion.vistas.base')

<!-- METAS -->
@section('titulo', 'Galeria - OverAlestur')
@section('description', 'Modulo de Galeria')

@section('componentes')
    <seccion-component titulo="Modulo de Informacion">
        <galeria-component></galeria-component>
    </seccion-component>
@endsection

@section('contenido')

@endsection
