@extends('administracion.vistas.base')


@section('titulo', 'Mensajes - OverAlestur')
@section('description', 'Modulo de Mensajes')

@section('componentes')
    <seccion-component titulo="Modulo de Mensajes">
        <mensajes-component></mensajes-component>
    </seccion-component>

@endsection

@section('contenido')

@endsection
