@extends('administracion.vistas.base')

@section('titulo', 'Tours - OverAlestur')
@section('description', 'Modulo de Tours')

@section('componentes')
    <seccion-component titulo="Modulo de Tures">
        <tures-component></tures-component>
    </seccion-component>
@endsection

@section('contenido')

@endsection