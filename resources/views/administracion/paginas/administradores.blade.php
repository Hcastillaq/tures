@extends('administracion.vistas.base')
<!-- METAS -->
@section('titulo', 'Administradores - OverAlestur')
@section('description', 'Modulo de administradores')

@section('componentes')
    <seccion-component titulo="Modulo de Administradores">
        <admins-component></admins-component>
    </seccion-component>
@endsection

@section('contenido')

@endsection