@extends('administracion.vistas.base')

<!-- METAS -->
@section('titulo', 'Informacion - OverAlestur')
@section('description', 'Modulo de Informacion')

@section('componentes')
    <seccion-component titulo="Modulo de Informacion">
        <informacion-component></informacion-component>
    </seccion-component>
@endsection

@section('contenido')

@endsection
