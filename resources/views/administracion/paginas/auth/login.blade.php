@extends('administracion.paginas.auth.base')

<!-- METAS -->
@section('titulo', 'Login - OverAlestur')
@section('description', 'Pagina de login para entrar a OverAlestur')

@section('componentes')
    <login-component csrf="{{ csrf_token() }}" 
        msg="{{ session('msg') }}"></login-component>
@endsection

@section('contenido')

@endsection