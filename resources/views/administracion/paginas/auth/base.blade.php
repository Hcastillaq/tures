<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')" />

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('administracion.partes.links')
</head>
<body>
    <div id="adminApp">
        @yield('componentes')
    </div>

    @yield('contenido')
</body>
</html>

@include('administracion.partes.scripts')
