<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('titulo')</title>
    <meta name="description" content="@yield('description')">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('administracion.partes.links')
</head>
<body>
    <div id="adminApp">
        <side-component :user="{{Auth::user()}}"></side-component>
        <toast-component :bus="adminBus"></toast-component>
        @yield('componentes')
    </div>

    @yield('contenido')
</body>
</html>

@include('administracion.partes.scripts')
