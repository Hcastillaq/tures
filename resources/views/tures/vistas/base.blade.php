<!DOCTYPE html>
<html lang="es-Es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('tituyietuldlo')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('inicio.partes.links')
</head>
<body>
    <div id="loadin_page">
        <img src="{{asset('imagenes/sitio')}}/loader.gif">
    </div>
    <div id="tours">
        @include('tures.partes.nav')
        @yield('components')
        <footers-component></footers-component>
    </div>
</body>
</html>
@include('tures.partes.scripts')