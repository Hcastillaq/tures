@extends('tures.vistas.base')

@section('components')

<div class="container" id="tures">
            <form action="" class="formulario-filtrar">

                <input class="inputs-formulario-rutas" type="text" 
                    name="origen" placeholder="Origen"
                    value="{{$origen}}">

                <input class="inputs-formulario-rutas" type="text" 
                    name="destino" placeholder="Destino" 
                    value="{{$destino}}">

                <input class="btn btn-enviar" type="submit" value="Filtrar">
                
            </form>
            <div class="row" id="content-tures">

                @if(count($tours) == 0)
                    ups
                @endif

                @forelse($tours as $tures)
                @if(count($tures->imagenes) != 0)
                    <a href="{{url('/tours')}}/{{$tures->id}}">
                        <div class="col xl4 m6 l3 card-tures">
                            <div class="card-info" style="background-image: url('{{asset('imagenes/tures')}}/{{$tures->imagenes[0]->url}}')"></div>
                            <div class="card-content-tour">
                                <div class="row">
                                    <strong>{{$tures->nombre}}</strong>
                                    <div class="info-tours">
                                        <span><i class="fa fa-home" aria-hidden="true"></i> {{$tures->origen}}</span>
                                        <span><i class="fa fa-map" aria-hidden="true"></i> {{$tures->destino}}<span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                @endif
                @empty
                @endforelse
            </div>
        </div>
@endsection