@extends('tures.vistas.base')

@section('components')
   
    <lightbox-component :bus="busData">
    </lightbox-component>
    
    <div class="container" id="tures">
        <div class="info-tour">
            <h5>{{$tour->nombre}}</h5>
            <span v-currency>{{$tour->precio}}</span>
        </div>

        <div class="content-tour">
            <div class="info-hito">
                <ul>
                    @forelse($tour->hitos as $hitos)
                        <li>
                            @if(!empty($hitos->titulo) || !empty($hitos->descripcion))
                                <strong>{{$hitos->titulo}}</strong>
                                <span>{{$hitos->descripcion}}</span>
                            @endif
                        </li>
                    @empty
                        <li>
                            <strong>Upps lo sentimos, aún no tenemos una ruta creada.</strong>
                        </li>
                    @endforelse
                </ul>
                <div class="rutaa">
                    @if(!empty($tour->origen) || !empty($tour->origen))
                        <p><i class="fa fa-home" aria-hidden="true"></i> {{$tour->origen}}</p>
                        <p><i class="fa fa-map" aria-hidden="true"></i> {{$tour->destino}}</p>
                    @endif
                </div>
            </div>
            <info class="sections-tour">
                <div class="mapa-tour">
                    {!! $tour->mapa !!}
                </div>
                <div class="galeria-tour">
                    <carrusel-component :galeria="{{$tour->imagenes}}" 
                        :tiempo="3000" :prefix="'tures/'">
                    </carrusel-component>
                </div>
            </info>
        </div>
    </div>
@endsection