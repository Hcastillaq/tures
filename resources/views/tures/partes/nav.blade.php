<navs-component url="{{url('/')}}" :clase=color>
    <div slot="uno">

        <ul id="nav-mobile" class="">
            <li>
                <a href="{{url('/')}}">
                    Inicio
                </a>
            </li>
            <li>
                <a href="{{url('/tours')}}">
                    Tours
                </a>
            </li>
            <li>
                <a href="{{url('/#nosotros')}}">
                    Nosotros
                </a>
            </li>
            <li>
                <a href="{{url('/#galeria')}}">
                    Galeria
                </a>
            </li>
            <li>
                <a href="{{url('/#contacto')}}">
                    Contacto
                </a>
            </li>
        </ul>
    </div>

    <div slot="dos">
        <ul id="nav-mobile" class="navMobile">
            <li>
                <a href="{{url('/')}}">
                    Inicio
                </a>
            </li>
            <li>
                <a href="{{url('/tours')}}">
                    Tours
                </a>
            </li>
            <li>
                <a href="{{url('/#nosotros')}}">
                    Nosotros
                </a>
            </li>
            <li>
                <a href="{{url('/#galeria')}}">
                    Galeria
                </a>
            </li>
            <li>
                <a href="{{url('/#contacto')}}">
                    Contacto
                </a>
            </li>
        </ul>
    </div>
</navs-component>