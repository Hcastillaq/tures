
require('./bootstrap');

window.Vue = require('vue');
window.adminBus = new Vue();

import sideComponent from './components/administracion/side.vue';
import seccionComponent from './components/administracion/seccion.vue';
import toastComponent from './components/toas.vue';

import turesComponent from './components/administracion/vistas/tures.vue';
import mensajesComponent from './components/administracion/vistas/mensajes.vue';
import galeriaComponent from './components/administracion/vistas/galeria.vue';
import informacionComponent from './components/administracion/vistas/informacion.vue';
import loginComponent from './components/administracion/vistas/login.vue';
import adminsComponent from './components/administracion/vistas/administradores.vue';


export default ()=>{
    const adminApp = new Vue({
        el:'#adminApp',
        components:{
            sideComponent,
            seccionComponent,
            turesComponent,
            mensajesComponent,
            galeriaComponent,
            informacionComponent,
            loginComponent,
            adminsComponent,
            toastComponent
        },
        data(){
            return{
                adminBus
            }
        }
    });
}
