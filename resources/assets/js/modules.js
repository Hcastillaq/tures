import tours from './rutas';
import index from './main';
import administracion from './administracion';

export default {
    tours: ()=>{
        tours()
    },

    index: ()=>{
        index()
    },

    administracion: ()=>{
        administracion()
    }

}