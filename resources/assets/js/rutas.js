import navsComponent from './components/tures/nav-rutas.vue';
import footersComponent from './components/tures/footer-tures.vue';
import carruselComponent from './components/carrusel.vue';
import lightboxComponent from './components/lightbox.vue';

import Vue from 'vue';
window.busData = new Vue();
import numeral from 'numeral';

    const reset = function(el){
        var number = numeral($(el).text());
        var string = number.format('0,0');
        $(el).html("<i class='fa fa-dollar' aria-hidden='true'></i> "+string+"  COP");
    }

    const currency = {

        update(el, binding, vnode){
            reset(el);
        },
        bind(el, binding, vnode){
            reset(el);
        }
    }
export default ()=>{   
    const app = new Vue({
        el:'#tours',
        components:{
            navsComponent,
            footersComponent,
            carruselComponent,
            lightboxComponent
        },
        data(){
            return{
                busData
            }
        },
        directives:{
            currency
        }
    });
}





