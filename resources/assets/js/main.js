require('./bootstrap');

window.Vue = require('vue');
window.busData = new Vue();

import navComponent from './components/nav.vue';
import footerComponent from './components/footer.vue';
import carruselComponent from './components/carrusel.vue';
import lightboxComponent from './components/lightbox.vue';
import contactoComponent from './components/contacto.vue';

export default ()=>{
    

    const app = new Vue({
        el: '#app',
        components:{
            navComponent,
            footerComponent,
            carruselComponent,
            lightboxComponent,
            contactoComponent
        },
        data:function(){
            return{
                color:'nav',
                busData
            }
        },
        mounted(){
            if(location.pathname == "/" || location.pathname == ""){
                const self = this;
                var altura = $(this.$el).find(".index-banner").height();
                var nav = $(this.$el).find("nav");
                var sombra = $(this.$el).find(".section-contac").position().top;
                var volver = $(this.$el).find(".section-mensaje").position().top;
                window.addEventListener("scroll",function(){
                    if(this.scrollY>(altura-nav.height())){
                        self.color="cambiar";
                        $("nav ul a").css("color", "black");
                    }else{
                        self.color="nav";
                        $("nav ul a").css("color", "white");
                    }
                });
            }
    
        }
    });
}