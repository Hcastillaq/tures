import  modulos from './modules.js';

const options = ['administracion', 'tours'];

let aPath = location.pathname.split("/");

aPath = aPath.filter( p => {
	if(p.trim().length > 0){
		return p
	}
});

let sw = false;

aPath.forEach(p => {
	options.forEach(o => {
		if(p == o && sw == false){
			modulos[o]();
			sw = true;
		}
	});
});

if(!sw){
	modulos['index']();
}