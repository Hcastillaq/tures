<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $table = 'galeria';
    protected $fillable = ['urlImagen', 'sitio_id'];

    public function sitio(){
    	return $this->belongsTo('App\Sitio');
    }
}
