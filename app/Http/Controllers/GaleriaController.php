<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Galeria;

class GaleriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imgs = Galeria::orderBy('id', 'DESC')->get();
        return $imgs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->file('imagenes') as $imagen) {
            $path = $imagen;
            $filename = date("d_m_Y_H_i_s").basename($path).
                '.'.$imagen->getClientOriginalExtension();

            $imagen->move(public_path('/imagenes/galeria'), $filename);

            /*
            $imagen = new Imagen();
            $imagen->url = $filename;
            $imagen->tour_id = $tour_id;
            $imagen->save();*/

            Galeria::create(['urlImagen'=>$filename]);
        }

        $imgs = Galeria::orderBy('id', 'DESC')->get();
        return $imgs;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $img = Galeria::find($id);
        if(!is_null($img) && !empty($img)){
            $img->delete();
        }
        return $img;
    }
}
