<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sitio;

class SitioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sitio = new Sitio();
        $sitio->nosotros = $request->input('nosotros');

        $imagen = $request->file('imagen');
        $path = $imagen;

        $filename = date("d_m_Y_H_i_s").basename($path).
            '.'.$imagen->getClientOriginalExtension();

        $imagen->move(public_path('/imagenes/sitio'), $filename);

        $sitio->imgLogo = $filename;
        $sitio->save();

        return $sitio;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return Sitio::all()->last();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $sitio = Sitio::all()->last();
        $sitio->nosotros = $request->input('nosotros');

        $imagen = $request->file('imagen');

        if(!is_null($imagen) && !empty($imagen)){
            $path = $imagen;

            $filename = date("d_m_Y_H_i_s").basename($path).
                '.'.$imagen->getClientOriginalExtension();

            $imagen->move(public_path('/imagenes/sitio'), $filename);

            $sitio->imgLogo = $filename;
        }

        $sitio->update();
        
        return $sitio;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sitio = Sitio::find($id);
        $sitio->delete();
        return $sitio;
    }
}
