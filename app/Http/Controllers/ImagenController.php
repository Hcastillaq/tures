<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imagen;
use App\Tour;

class ImagenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $t = Tour::find($request->input('tour_id'));

        foreach ($request->file('imagenes') as $imagen) {
            $path = $imagen;
            $filename = date("d_m_Y_H_i_s").basename($path).
                '.'.$imagen->getClientOriginalExtension();

            $imagen->move(public_path('/imagenes/tures'), $filename);

            /*
            $imagen = new Imagen();
            $imagen->url = $filename;
            $imagen->tour_id = $tour_id;
            $imagen->save();*/

            $t->imagenes()->create(['url'=>$filename]);


        }
        return $t->imagenes;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $img = Imagen::find($id);
        $img->delete();
        return $img;
    }
}
