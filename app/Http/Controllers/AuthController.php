<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Administrador as admin;

class AuthController extends Controller
{
    //
    public function login(Request $request){

        $login = Auth::attempt([
            'usuario'=>$request->input('usuario'),
            'password'=>$request->input('password')
        ], false);

        if($login){
            return redirect('/administracion');
        }

        return redirect('/administracion/entrar')->with('msg', 'Error en los datos');
    }

    public  function logout(){
        if(Auth::check()){
            Auth::logout();
        }
        return redirect('/administracion/entrar');
    }
}
