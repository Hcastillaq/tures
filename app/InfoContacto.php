<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoContacto extends Model
{
    protected $table = 'infoContacto';
    protected $fillable = ['telefono', 'correo', 'sitio_id', 'direccion'];

    public function sitio(){
    	return $this->belongsTo('App\Sitio');
    }
}
