<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $table = 'tours';
    protected $fillable = ['nombre', 'precio', 'tipo', 'mapa', 'destino', 'origen'];

    public function hitos(){
    	return $this->hasMany('App\Hito');
    }

    public function imagenes(){
    	return $this->hasMany('App\Imagen');
    }
}
