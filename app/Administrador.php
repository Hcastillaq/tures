<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Administrador extends Authenticatable
{
    protected $table = 'administradores';
    protected $fillable = ['usuario', 'nombre', 'password', 'range'];

    public function sitio(){
    	return $this->belongsTo('App\Sitio');
    }
}
