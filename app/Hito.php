<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hito extends Model
{
    protected $table = 'hitos';
    protected $fillable = ['titulo', 'descripcion', 'tour_id'];

    public function tour(){
    	return $this->belongsTo('App\Tour');
    }
}
