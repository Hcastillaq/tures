<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sitio extends Model
{
    protected $table = 'sitio';
    protected $fillable = ['nosotros', 'imgLogo'];

    public function infoContacto(){
    	return $this->hasOne('App\InfoContacto');
    }

    public function galeria(){
    	return $this->hasOne('App\Galeria');
    }

    public function administradores(){
    	return $this->hasOne('App\Administrador');
    }

    public function mensajes(){
    	return $this->hasOne('App\Mensaje');
    }
}
