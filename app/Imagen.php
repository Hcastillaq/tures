<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    protected $table = 'imagenes';
    protected $fillable = ['url', 'tour_id'];

    public function tour(){
    	return $this->belongsTo('App\Tour');
    }
}
