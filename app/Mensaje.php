<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    protected $table = 'mensajes';
    protected $fillable = ['nombre', 'correo', 'mensaje'];

    public function sitio(){
    	return $this->belongsTo('App\Sitio');
    }
}
